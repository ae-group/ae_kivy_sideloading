<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# kivy_sideloading 0.3.24

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_sideloading/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_sideloading)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_sideloading/release0.3.23?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_sideloading/-/tree/release0.3.23)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_sideloading)](
    https://pypi.org/project/ae-kivy-sideloading/#history)

>ae_kivy_sideloading package 0.3.24.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_sideloading/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_sideloading/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_sideloading/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_sideloading/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_sideloading/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_sideloading/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_sideloading)](
    https://gitlab.com/ae-group/ae_kivy_sideloading/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_sideloading)](
    https://gitlab.com/ae-group/ae_kivy_sideloading/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_sideloading)](
    https://gitlab.com/ae-group/ae_kivy_sideloading/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_sideloading)](
    https://pypi.org/project/ae-kivy-sideloading/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_sideloading)](
    https://gitlab.com/ae-group/ae_kivy_sideloading/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_sideloading)](
    https://libraries.io/pypi/ae-kivy-sideloading)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_sideloading)](
    https://pypi.org/project/ae-kivy-sideloading/#files)


## installation


execute the following command to install the
ae.kivy_sideloading package
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-sideloading
```

if you want to contribute to this portion then first fork
[the ae_kivy_sideloading repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_sideloading "ae.kivy_sideloading code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_sideloading):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_sideloading/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_sideloading.html
"ae_kivy_sideloading documentation").
