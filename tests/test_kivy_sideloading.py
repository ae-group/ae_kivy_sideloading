""" unit tests """
from ae.kivy_sideloading import SideloadingMainAppMixin, SideloadingMenuPopup


def test_import():
    assert SideloadingMainAppMixin
    assert SideloadingMenuPopup
